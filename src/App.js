import './App.css';

import React, { Component } from 'react';
import { BrowserRouter, Route, Routes } from 'react-router-dom';

import Home from './Home';

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/wegotpagescommander" element={<Home />} />
          <Route path={process.env.PUBLIC_URL} element={<Home />} />
        </Routes>
      </BrowserRouter>
    );
  }
}

export default App;