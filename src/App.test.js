import { render, screen } from '@testing-library/react';
import App from './App';

beforeEach(() => {
  render(<App />);
});

let linkElement = null;

test('renders the game name', () => {
  linkElement = screen.getByText(/We Got Missiles Commander/i);
  expect(linkElement).toBeVisible();
});

test('render the missile icon', () => {
  linkElement = screen.getByAltText(/logo/i);
  expect(linkElement).toBeVisible();
});

test('verify the external play store link', () => {
  linkElement = document.getElementById('googleplay');
  expect(linkElement).toBeVisible();

  const url = linkElement.getAttribute("href")
  expect(url).toBeTruthy();

  const response = fetch(url);
  expect(response).toBeTruthy();
});